import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;

public class CBApplication {
	
	private static WebDriver driver;
	static String baseurl;
	
	public static void main(String srgs[]) throws InterruptedException, SQLException, ClassNotFoundException, ParseException{
		System.setProperty("webdriver.firefox.marionette","D:\\1\\7Testing\\geckodriver.exe");
				
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.download.dir", "/home/dev/Documents/Wget");
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk", 
			    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"); 
	 
		profile.setPreference( "browser.download.manager.showWhenStarting", false );
		profile.setPreference( "pdfjs.disabled", true );
		
		driver=new FirefoxDriver();
		baseurl="http://adportal.careerbliss.com/Home/LogOn";
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		
		
		driver.get(baseurl);
		
		
	    driver.findElement(By.id("loginBtn")).click();
	    driver.findElement(By.id("loginEmail")).clear();
	    driver.findElement(By.id("loginEmail")).sendKeys("jason@jasonstevens.ca");
	    driver.findElement(By.id("loginPassword")).clear();
	    driver.findElement(By.id("loginPassword")).sendKeys("FBGJason");
	    driver.findElement(By.cssSelector("input.image")).click();
	    driver.findElement(By.linkText("Dart Alerts")).click();
	    driver.findElement(By.id("reportBtn")).click();
	    
	    
		
	    WebElement table = driver.findElement(By.cssSelector("#campaignReportTable"));
	    
	    List<WebElement> allRows = table.findElements(By.tagName("tr"));
	    
	    
	    String val = "";
	    ArrayList<String> valList = new ArrayList<String>();
	    
		for (WebElement row : allRows) {
			List<WebElement> cells = row.findElements(By.tagName("td"));
			val = "";
			for (WebElement cell : cells) {
				val = val+"|"+cell.getText();
			}
			
			if(!val.trim().equals("")){
				val = val.substring(1);				
				valList.add(val);
			}
		}

		
		
		ArrayList<String> dateList = com.cb.handler.CBHandler.getDateList();
		
		String date = "";
		int jobCount = 0;
		int clicks = 0;
		int impressions = 0;
		int ctr = 0;
		int avgCpc = 0;
		String cost = "";
		int conversions = 0;
		String[] tabelVals = null;
		String qryvals = "";
		for(String x : valList){
			tabelVals = x.split("\\|");
			
			if(tabelVals[0].trim().equals("")){ continue; }
			
			date = tabelVals[0];
			date  = com.cb.util.Utility.convertDate(date);
			impressions = Integer.parseInt(tabelVals[1].replace(",", ""));
			clicks = Integer.parseInt(tabelVals[2].replace(",", ""));
			jobCount = 0;
			cost = tabelVals[5].replace("$", "").replace(",", "");
			
			//Date check, only insert new record
	        if(dateList!=null && dateList.contains(date)){ continue; }
	        
	        //Current Date Check
	        if(date.equals(com.cb.util.Utility.currentDate())){ continue; }

	        
	        //Insert into DB
			System.out.println("date: "+date);
			System.out.println("jobCount: "+jobCount);
			System.out.println("clicks: "+clicks);
			System.out.println("impressions: "+impressions);
			System.out.println("ctr: "+ctr);
			System.out.println("avgCpc: "+avgCpc);
			System.out.println("cost: "+cost);
			System.out.println("conversions: "+conversions);
			qryvals = qryvals+","+"(null, "+clicks+", "+cost+", "+impressions+", "+jobCount+", 'CB-jobExport', '"+date+"' )";
			System.out.println("----------------------------------");
			
		}
	    
	    
	    
	  //Insert into DB
		String qry = "";
		if(!qryvals.trim().equals("")){
			qryvals = qryvals.substring(1);
			Statement st = com.cb.connection.Config.Connect().createStatement();
			qry = "INSERT INTO tbl_job_export_cost_stats "
					+ "(id, clicks, cost, impressions, jobs_count, export_source, date) "
					+ "VALUES "+qryvals;
			System.out.println(qry);
			
			st.executeUpdate(qry);
			com.cb.connection.Config.Close();
		}
		
		System.out.println("Done");
	}

}
