package com.cb.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CBHandler {
	
	public static ArrayList<String> getDateList() throws ClassNotFoundException, SQLException{
		ArrayList<String> dataList = new ArrayList<String>();

		Statement st = com.cb.connection.Config.Connect().createStatement();
		ResultSet rs = st.executeQuery("select distinct date from tbl_job_export_cost_stats where export_source='cb-jobExport';");		
		while(rs.next()){	
			if(rs.getString("date")==null || rs.getString("date").trim().equals("")){ continue; }
			
			dataList.add(rs.getString("date"));
		}
		
		com.cb.connection.Config.Close();
		
		return dataList;
	}

}
